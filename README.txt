Data Combiner
============================
* Google Spreadsheet Add-in used to combine data with quique keys.
* V2.0
* Created by Devin Hunter (devin.hunter@gmail.com)

Setup
--------------------
1. Open Script editor.
2. Create additional HTML pages
	* PageList.html
	* SidebarJavaScript.html
	* Stylesheet.html
	* Condense.html
	* Merge.html
3. Copy contents of files into appropriate page.
4. Save and close spreadsheet.
5. Open spreadsheet
6. Run Add-On and authorize.
7. Done

Contributers
---------------------
* Devin Hunter (Creator) - devin.hunter@gmail.com

Google Draft Product info
---------------------
## Title
Data Combiner

### Description
Combine spreadsheet data by picking a column to detect unique objects that are listed on multiple data rows.

### Terms of service URL
https://bitbucket.org/Doltknuckle/googledatacombiner/wiki/Home

### Help URL


### Report Issues URL
https://bitbucket.org/Doltknuckle/googledatacombiner/issues

### Post-install tip
Click "Add-ons > DataCombiner > Condense Sheet" to process your data and combine duplicate rows. The results of the process will be displayed on a new sheet.

### Product Key
MGekohisy2WDCzceOWY6JnKarw4f19nBj

Google Marketplace Info
--------------------
###Detailed Description
Combine Spreadsheet data by identifying a column that holds a data key. Rows with the matching data keys will be combined. This tool allows you to configure how the data is combined. Options include:
- Return First/Last Cell
- Append Data to the start/end
- Sum number
- Return Largest/Smallest number

The data will be written to a new sheet.


Revision History
------------------------
v2.1 - Bug fix
* Fixed separator character bug for Combine Rows. (#25)

v2.0 - Code rewrite to allow fot sheet Merges
* Non-data parameters stored in a single object.
* Logging now has a simple/verbose/off setting.
* Removed uneeded functions
* Key column is now stored as an interger.
* NEW FEATURE: Merge sheets

v1.2 - Google Review round 2
* Renamed buttons (#18, #19)
* Standardized text (#17)
* Column count will show header when headers exist (#16)
* Added sheet controls (#15, #20)
* Change Matching Data Column on Data Logic page to text (#21)

v1.1 - Google Review round 1
* Standardized HTML object IDs
* Renamed "Open Tool" to "Condense Sheet"
* Fixed Logging bug (Maximum call stack size)
* Renamed Open tool to "Condense Sheet (#3)
* Renamed Key data to "Matching Data" Rewrote coment. (#13)
* Moved Autodetect buttons to a better location.
* Rearranged Action Bar. (#11, #6, #9)
* Reworded Data Processed page (#12)
* Converted row count checkbox to toggle.
* Added Sort data toggle.
* Removed Delete Results button.
* Implemented sort feature (#5)
* Reworked logic selection. (#14)
* Added result sheet to report page (#7)

v1.0
* Added logging toggle
* Removed unused styles
* Prepared for Google Product review.

v0.2
* Added Result Deletion.
* Added a Finish button at end of process.
* Clicking Back will not reset interface of previous page.
* Added Column Count option.
* Combine Columns will now only append unique data.

v0.1 - Inital Build
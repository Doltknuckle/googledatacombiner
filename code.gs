/**
 * @OnlyCurrentDoc  Limits the script to only accessing the current spreadsheet.
 */

//**Global Variables**//
var SIDEBAR_TITLE = 'Data Combiner';
var SIDEBAR_PAGE_DEFAULT = "Condense";

//**Interface Functions**//

//* onOpen - Adds custom menu to google interface
function onOpen(e) {
  SpreadsheetApp.getUi()
      .createAddonMenu()
      .addItem('Condense Sheet', 'menuActionCondense')
      .addItem('Merge Sheets', 'menuActionMerge')
      .addToUi();
}

//* menuActionCondense - Open Condense sheet sidebar
function menuActionCondense(){
  showSidebar("Condense Sheet");
}

//* menuActionMerge - Open merge sheets sidebar
function menuActionMerge(){
  showSidebar("Merge Sheets");
}

//* showSidebar - Opens sidebar menu
function showSidebar(updatePage) {
  var currentPage = SIDEBAR_PAGE_DEFAULT;
  var currentTitle = SIDEBAR_TITLE;
  if(updatePage){
    currentPage = updatePage.substring(0,updatePage.indexOf(" "));
    currentTitle = updatePage;
  }
  var ui = HtmlService.createTemplateFromFile(currentPage)
    .evaluate()
    .setTitle(currentTitle);
  ui.setSandboxMode(HtmlService.SandboxMode.IFRAME);
  SpreadsheetApp.getUi()
    .showSidebar(ui);
}

//**Transactional Functions**//

//* changeSheet - switch active sheet to a new one.
function changeSheet(sheetName){
  var spreadSheet = SpreadsheetApp.getActiveSpreadsheet();
  
  //Change Active Sheet
  SpreadsheetApp.setActiveSheet(spreadSheet.getSheetByName(sheetName));
}

//* deleteSheetByName - Delete result sheets
function deleteSheetByName(sheetName){
  var file = SpreadsheetApp.getActiveSpreadsheet()
  var sheets = file.getSheets();
  
  for (var i=0; i < sheets.length; i++){
    //Loop sheets
    var tempName = sheets[i].getName()
    if(tempName.indexOf(sheetName) > -1){
      //Sheet matches name, delete
      if(tempName != sheetName){file.deleteSheet(sheets[i]);}
    }
  }
}

//* getActiveSheetName - Return the name of the active sheet
function getActiveSheetName(){
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
  return sheet.getSheetName();
}

//* getDataRange - Pull dataRange from active spreadsheet
function getDataRange(targetAction){
  var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
  var range = sheet.getActiveRange();
  if (targetAction == "dataLastCell" || targetAction == "actionLastCell"){
    //Get the range that includes the last cell with content.
    var lastRow = sheet.getLastRow();
    var lastColumn = sheet.getLastColumn();
    var range = sheet.getRange(1, 1, lastRow, lastColumn); 
    sheet.setActiveRange(range);   
  }
 
  return range.getA1Notation() || "A1";
}

//* getDataValues - Pull data out of the active sheet.
function getDataValues(sheetName,activeRange,limit){
  Logger.log("getDataValues("+sheetName+", "+activeRange+", "+limit+")");
  var file = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = file.getSheetByName(sheetName);
  var baseRange = sheet.getRange(activeRange);
  
  if(limit > 0){
    var tempRange = sheet.getRange(baseRange.getRow(), baseRange.getColumn(), limit, baseRange.getNumColumns());
    baseRange = tempRange;
    Logger.log("-New Range: "+baseRange.getA1Notation());
  }
  var returnObject = baseRange.getValues();
  return returnObject;
}

//* getSheetList(); - Gets array of sheet names, with the active one at the end.
function getSheetList(addActive){
  var returnObject = [];
  var sheetList = SpreadsheetApp.getActiveSpreadsheet().getSheets();
  for (var i=0; i < sheetList.length; i++){
    returnObject.push(sheetList[i].getSheetName());
  }
  //Add active sheet to end
  if(addActive){returnObject.push(getActiveSheetName());}
  
  return returnObject;
}

//* sortData - Sorts data by a key row.
function sortData(sheetName,activeRange,keyColumn,hasHeader){
  var file = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = file.getSheetByName(sheetName);
  var baseRange = sheet.getRange(activeRange)
  var headerSkip = 0;

  //Check for header
  if(hasHeader){headerSkip++;}
  
  //Sort List
  var sortRange = sheet.getRange((1+headerSkip),baseRange.getColumn(),(baseRange.getNumRows()-headerSkip),baseRange.getNumColumns());
  sortRange.sort(keyColumn);
  
  return "Sort Complete on Column: #" + keyColumn;
}

//* writeDataList - Write data to a new sheet
function writeDataList(sheetName, globalWriteList){
  var file = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = file.getSheetByName(sheetName);
  var sheetIndex = sheet.getIndex();
  
  var length = globalWriteList.length;
  var width = globalWriteList[0].length;
  
  //Create result sheet
  for (var i=1;i<=10;i++){
    //Check for duplicate Name
    var sheetNameBin = sheetName + " (Result#" + i +")";
    var testSheet = file.getSheetByName(sheetNameBin);
    if(testSheet == null){
      break;
    }
  }
  Logger.log("--Add Sheet: " + sheetNameBin);
  file.insertSheet(sheetNameBin, sheetIndex)
  var newSheet = file.getSheetByName(sheetNameBin);
  
  //Set DataRange
  var tempRange = newSheet.getRange(1, 1, length, width);
  tempRange.setValues(globalWriteList); 
  
  return sheetNameBin;
}

//**Sub Function Library**//

//* onInstall - make sure that onOpen runs
function onInstall(e) {
  onOpen(e);
}